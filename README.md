# FIREBASE CLOUD FUNCTION GAZELOOK
Cloud functions desarrolladas para notificaciones

# TECNOLOGÍAS
Para éste proyecto utilizarémos las sguientes tecnologías

Node: 13.8.0

TypeScript: 3.8.0

Firebase 9.2.2


## INSTALACIÓN

  Instalar las dependencias
  ```
  npm install
  ```
## EJECUCIÓN

  Compilar el proyecto
  ```
  npm run build
  ```

  Iniciar sesión con la cuenta del proyecto de firebase
  ```
  firebase login
  ```

  Emular localmente
  ```
  firebase emulators:start
  ```

## DEPLOY A PRODUCCIÓN
  Ejecutar este comando para implementar las funciones:
  ```
  firebase deploy --only functions
  ```







