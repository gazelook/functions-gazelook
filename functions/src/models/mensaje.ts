import { Participantes } from "./asociacion";
import { Conversacion } from "./conversacion";
import { Perfil } from "./perfil";

export interface Propietario {
    id: string;
    perfil: Perfil;
}

export interface TipoMensaje {
    codigo: string;
}

export interface EstadoMensaje {
    codigo: string;
}

export interface Mensaje {
    id: string;
    propietario: Propietario;
    tipo: TipoMensaje;
    conversacion: Conversacion;
    leidoPor: Array<Participantes>;
}