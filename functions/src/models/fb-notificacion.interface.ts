export interface FBNotificacion {
    idEntidad: string;
    leido: boolean;
    data: any;
    codEntidad: string;
    estado: string;
    accion: string;
    query: any;
}