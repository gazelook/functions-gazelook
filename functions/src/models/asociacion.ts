import { Perfil } from "./perfil";

export interface Participantes {
    id: string;
    perfil: Perfil;
}

export interface Asociacion {
    id: string;
    participantes: Array<Participantes>;
}