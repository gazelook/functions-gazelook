import { Asociacion } from "./asociacion";
import { Mensaje } from "./mensaje";

export interface Conversacion {
    id: string;
    asociacion: Asociacion;
    ultimoMensaje: Mensaje;
}