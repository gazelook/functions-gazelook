import * as functions from "firebase-functions";
import * as admin from "firebase-admin";
import { Mensaje } from "./models/mensaje";
import { Conversacion } from "./models/conversacion";
import { accionNotificacionFirebase, codigoEntidades, estadoNotificacionesFirebase } from "./shared/enum-sistema";
import { FBNotificacion } from "./models/fb-notificacion.interface";


admin.initializeApp(functions.config().firebase);
const fireDatabase = admin.database();

exports.notificarUltimoMensaje = functions.database.ref("conversaciones/{uid}/ultimoMensaje").onUpdate(async (event: any) => {
    let ultimoMensaje: Mensaje;
    ultimoMensaje = event.after._data;

    //obtener datos de la conversacion
    const getConversacion = await fireDatabase.ref(`conversaciones/${ultimoMensaje.conversacion.id}`).get();
    console.log("key CONVERSACION:", getConversacion.key);
    const dataConversacion: Conversacion = getConversacion.val();

    // convert json Participantes to Array
    let resultParticipantes: any = [];
    const participantes = dataConversacion.asociacion.participantes;
    for (const key in participantes) {
        if (participantes.hasOwnProperty(key)) {
            resultParticipantes.push(participantes[key]);
        }
    }
    dataConversacion.asociacion.participantes = resultParticipantes;

    //convert json leidopor(ultimoMensaje) to Array
    let resultLeidoPor: any = [];
    const leidoPor = ultimoMensaje.leidoPor;
    for (const key in leidoPor) {
        if (leidoPor.hasOwnProperty(key)) {
            resultLeidoPor.push(leidoPor[key]);
        }
    }
    ultimoMensaje.leidoPor = resultLeidoPor;

    // ----------------------------- NOTIFICAR -------------------------------
    let dataNotificacion: FBNotificacion;

    const notifications = fireDatabase.ref().child('notificaciones');

    const participantesAsoc = dataConversacion.asociacion.participantes;

    // filtrar quienes no han leido el mensaje
    const filtroParticipantesNotificar = participantesAsoc.filter((item: any) => {
            for (const leido of ultimoMensaje.leidoPor) {
                if (item.id !== leido.id) {
                    return item;
                }
            }
        });

    // enviar notificacion
    if (filtroParticipantesNotificar.length > 0) {
        for (const participante of filtroParticipantesNotificar) {
            dataNotificacion = {
                idEntidad: dataConversacion.asociacion.id,
                leido: false,
                data: dataConversacion.ultimoMensaje,
                codEntidad: codigoEntidades.entidadMensaje,
                estado: estadoNotificacionesFirebase.activa,
                accion: accionNotificacionFirebase.ver,
                query: `${codigoEntidades.entidadMensaje}-${false}`
            }
            // guardar la notificacion
            notifications.child(codigoEntidades.entidadPerfiles).child(participante.perfil._id).push(dataNotificacion);
        }
    }
});
