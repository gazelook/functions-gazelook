
export enum codigoEntidades {
    entidadUsuarios = 'ENT_1',
    entidadProyectos = 'ENT_2',
    entidadNoticias = 'ENT_3',
    entidadPensamientos = 'ENT_4',
    entidadTransaccion = 'ENT_6',
    entidadPerfiles = 'ENT_8',
    entidadcontactos = 'ENT_9',
    entidadComentarios = 'ENT_10',
    entidadMedia = 'ENT_12',
    entidadDispositivo = 'ENT_13',
    entidadBeneficiario = 'ENT_18',
    entidadTelefono = 'ENT_20',
    entidadDireccion = 'ENT_21',
    entidadAsociacion = 'ENT_22',
    entidadParticipanteAsociacion = 'ENT_23',
    entidadConversacion = 'ENT_24',
    entidadMensaje = 'ENT_25',
    entidadVotoProyecto = 'ENT_31',
    entidadParticipanteProyecto = 'ENT_32',
    entidadEstrategia = 'ENT_34',
    entidadSuscripcion = 'ENT_38',
    entidadAlbum = 'ENT_42',
    entidadCatalogoLocalidad = 'ENT_46',
    entidadTraduccionNoticia = 'ENT_47',
    entidadVotoNoticia = 'ENT_51',
    entidadTraduccionProyecto = 'ENT_55',
    fondosFinanciamiento = 'ENT_95',
    fondosTipoProyecto = 'ENT_96',
    entidadBonificacion = 'ENT_97',
    intercambio = 'ENT_100',
    traduccionIntercambio = 'ENT_101',
    catalogoTipoIntercambio = 'ENT_102',
    traduccionCatalogoTipoIntercambio = 'ENT_103',
    entidadNotificacionesFirefase = 'ENT_107',
}

export enum accionNotificacionFirebase {
    ver = 'ACCNOTFIR_1'
}

export enum estadoNotificacionesFirebase {
    activa = 'EST_241',
    eliminado = 'EST_242',
}